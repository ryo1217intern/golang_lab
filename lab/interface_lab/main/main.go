package main

import (
	"interface_lab/interfaces"
)

func main() {
	// Create a human and a dog
	h := interfaces.NewHuman("Taro", 20, "right")
	d := interfaces.NewDog("Chihuahua", "Pochi", "left")

	// Call the methods

	// The human takes the first and second steps
	h.FirstStepWalk()
	h.SecondStepWalk()

	// The dog takes the first and second steps
	d.FirstStepWalk()
	d.SecondStepWalk()

	hh := interfaces.NewHuman("Ryosuke", 22, "right")
	interfaces.HowToWalk(hh)
}