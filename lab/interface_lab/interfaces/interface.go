package interfaces

import "fmt"

// Interface
type  IWaker interface {
	FirstStepWalk() error
	SecondStepWalk() error
}

// human struct
type Human struct {
	Name string
	Age  int
	Dominant string
}

// Dog struct
type Dog struct {
	Kind string
	Name string
	Dominant string
}

// Human constructor
func NewHuman(name string, age int, dominant string) *Human {
	return &Human{name, age, dominant}
}

// Dog constructor
func NewDog(kind string, name string, dominant string) *Dog {
	return &Dog{kind, name, dominant}
}

// Human methods FirstStepWalk
func (h *Human) FirstStepWalk() error {
	// dominant is the foot that the person uses to take the first step
	if h.Dominant == "right"	{
		fmt.Printf("%sは最初の一歩は左足\n", h.Name)
	} else if h.Dominant == "left"{
		fmt.Printf("%sは最初の一歩は右足\n", h.Name)
	}
	return nil
}

// Human methods SecondStepWalk
func (h *Human) SecondStepWalk() error {
	// dominant is the foot that the person uses to take the second step
	if h.Dominant == "right"	{
		fmt.Printf("%sは二歩目は右足\n", h.Name)
	} else if h.Dominant == "left"{
		fmt.Printf("%sは二歩目は左足\n", h.Name)
	}
	return nil
}

// Dog methods FirstStepWalk
func (d *Dog) FirstStepWalk() error {
	// dominant is the foot that the dog uses to take the first step
	if d.Dominant == "right"	{
		fmt.Printf("%sは最初の一歩は左前足を前に出しながら右前足を後ろに引く\n", d.Name)
	} else if d.Dominant == "left"{
		fmt.Printf("%sは最初の一歩は右前足を前に出しながら左前足を後ろに引く\n", d.Name)
	}
	return nil
}

// Dog methods SecondStepWalk
func (d *Dog) SecondStepWalk() error {
	// dominant is the foot that the dog uses to take the second step
	if d.Dominant == "right"	{
		fmt.Printf("%sは二歩目は右前足を前に出しながら左前足を後ろに引く\n", d.Name)
	} else if d.Dominant == "left"{
		fmt.Printf("%sは二歩目は左前足を前に出しながら右前足を後ろに引く\n", d.Name)
	}
	return nil
}

func HowToWalk(w IWaker) {
	w.FirstStepWalk()
	w.SecondStepWalk()
}
