package functions

import "fmt"

//メモリアドレスをレスポンスする関数.
func ResponseRamAddress() *int {
	//Responseをint型で定義
	var Response int = 1

	//Responseを表示
	fmt.Printf("Response: %d\n",Response)

	//Responseのメモリアドレス
	ResponseAddress := &Response

	//Responseのメモリアドレスを表示
	fmt.Printf("ResponseAddress: %p\n", ResponseAddress)

	//intのポインタ型を返す
	return &Response
}