package main

import (
	"fmt"
	"pointer_lab/functions"
)

func main() {
	//ResponseRamAddress関数を実行
	receiveResponse := functions.ResponseRamAddress()

	//receiveResponse: ポインタ型 メモリアドレスから値を参照する.
	fmt.Printf("*receiveResponse: %d\n",*receiveResponse)

	//変数のメモリアドレスを参照して値を書き換える.
	*receiveResponse = 2

	//書き換えた後の値を参照する.
	fmt.Printf("*receiveResponse: %d\n",*receiveResponse)
}