package main

import (
	"fmt"
	"struct_lab/structs"
)

func main() {
	// Create a new person
	var human structs.Person

	// Set the name and age of the person
	human.Name = "ryosuke"
	human.Age = 22

	// Print the name and age of the person
	fmt.Printf("human.Name: %s human.Age: %d \n",human.Name, human.Age)

	// Create a new person
	p := structs.NewPerson("John", 25)

	// Print the name and age of the person
	fmt.Printf("p.Name: %s p.Age: %d \n",p.Name, p.Age)

	// Set the name of the person
	p.SetName("Doe")

	// Set the age of the person
	p.SetAge(30)

	// Print the name and age of the person
	fmt.Printf("p.Name: %s p.Age: %d \n",p.Name, p.Age)
}