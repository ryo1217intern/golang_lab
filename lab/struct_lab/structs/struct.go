package structs

// Person is a struct that represents a person
type Person struct {
	Name string
	Age  int
}

// PersonInterface is an interface for the Person struct
type PersonInterface interface {
	SetName(name string)
	SetAge(age int)
}

// NewPerson is a constructor for Person
func NewPerson(name string, age int) *Person {
	return &Person{name, age}
}

// GetName returns the name of the person
func (p *Person) SetName(name string) {
	p.Name = name
}

// GetAge returns the age of the person
func (p *Person) SetAge(age int) {
	p.Age = age
}