package main

import (
	"fmt"
	"module_lab/modules"
)

func main() {
	// Print a message
	fmt.Println("Hello, I am a main function from the main package.")

	// Call the SpeakSubMainFunction from the main package
	SpeakSubMainFunction()

	// Call the SpeakModulesFunction from the modules package
	modules.SpeakModulesFunction()
}