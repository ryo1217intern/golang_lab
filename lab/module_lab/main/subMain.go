package main

import "fmt"

func SpeakSubMainFunction() {
    fmt.Println("Hello, I am a subMain function from the main package.")
}