package modules

import "fmt"

func SpeakModulesFunction() {
	// Print a message
	fmt.Println("Hello, I am a SpeakModulesFunction from the modules package.")
}