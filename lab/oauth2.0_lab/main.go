package main

import (
	"context"  // コンテキストを管理するためのパッケージ
	"log"      // ログ出力のためのパッケージ
	"net/http" // HTTPサーバーとクライアントの実装
	"os"       // OS関連の操作として環境変数の取得

	"github.com/coreos/go-oidc"   // OpenID Connectのクライアント
	"github.com/joho/godotenv"    // .envファイルから環境変数を読み込むためのパッケージ
	"github.com/labstack/echo/v4" // Echoフレームワーク
	"golang.org/x/oauth2"         // OAuth 2.0のクライアント
	"golang.org/x/oauth2/google"  // GoogleのOAuth 2.0サービス
)

// グローバル変数の定義
var (
	clientID     string
	clientSecret string
	redirectURL  = "http://localhost:8081/callback"
	provider     *oidc.Provider
	config       *oauth2.Config
)

func init() {
	// .envファイルから環境変数をロード
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	// 環境変数からクライアントIDとクライアントシークレットを取得
	clientID = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")

	// GoogleのOpenID Connectプロバイダーを初期化
	var err error
	provider, err = oidc.NewProvider(context.Background(), "https://accounts.google.com")
	if err != nil {
		log.Fatalf("failed to get provider: %v", err)
	}

	// OAuth 2.0クライアント設定の初期化
	config = &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		Endpoint:     google.Endpoint,
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
}

func main() {
	e := echo.New() // Echoインスタンスの作成

	// ハンドラ関数のマッピング
	e.GET("/", handleMain)
	e.GET("/login", handleLogin)
	e.GET("/callback", handleCallback)

	// サーバーの起動
	log.Fatal(e.Start(":8081"))
}

// メインページのハンドラ関数
func handleMain(c echo.Context) error {
	return c.HTML(http.StatusOK, "Hello! Please <a href='/login'>log in</a>")
}

// ログインページのハンドラ関数
func handleLogin(c echo.Context) error {
	// 認証URLを生成しリダイレクト
	url := config.AuthCodeURL("state", oauth2.AccessTypeOffline)
	return c.Redirect(http.StatusFound, url)
}

// OAuthコールバックのハンドラ関数
func handleCallback(c echo.Context) error {
	ctx := context.Background()
	// 認証コードをトークンに交換
	oauth2Token, err := config.Exchange(ctx, c.QueryParam("code"))
	if err != nil {
		return c.String(http.StatusInternalServerError, "Failed to exchange token: "+err.Error())
	}

	// IDトークンの取得
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		return c.String(http.StatusInternalServerError, "No id_token field in oauth2 token.")
	}

	// IDトークンの検証
	idToken, err := provider.Verifier(&oidc.Config{ClientID: clientID}).Verify(ctx, rawIDToken)
	if err != nil {
		return c.String(http.StatusInternalServerError, "Failed to verify ID Token: "+err.Error())
	}

	// ユーザー情報の取得
	var profile map[string]interface{}
	if err := idToken.Claims(&profile); err != nil {
		return c.String(http.StatusInternalServerError, "Failed to get user profile: "+err.Error())
	}

	// ドメインの検証
	if domain, ok := profile["hd"].(string); !ok || domain != "dev-ryo.com" {
		return c.String(http.StatusUnauthorized, "Unauthorized domain")
	}

	// ログイン成功メッセージの表示
	return c.String(http.StatusOK, "Login successful: "+profile["email"].(string))
}
