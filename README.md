# Golang_lab

## 目的

Golang の実験場（学習）を目的としたリポジトリを作成し、実験を行う.

また裏目標として Udemy にて学習している go-rest-api のコードを理解することを目的にしている.

現在詰まってしまっていることは以下の点.

- ポインターについて
- Di(依存性の注入)を行うための interface について
- コンストラクタについて
- golang 独特な module 管理について

## Golang の環境構築

```
brew install go
```

## Dir の構成について

```Dir
.
├── README.md
├── changelog.config.js
├── lab
│   ├── interface_lab
│   │   └── go.mod
│   ├── module_lab
│   │   ├── go.mod
│   │   └── modules
│   │       └── module.go
│   ├── pointer_lab
│   │   ├── functions
│   │   │   └── pointer.go
│   │   ├── go.mod
│   │   └── main
│   │       └── main.go
│   └── struct_lab
│       ├── go.mod
│       ├── main
│       │   └── main.go
│       └── structs
│           └── struct.go
├── node_modules
│   └── git-cz
│       ├── CHANGELOG.md
│       ├── README.md
│       ├── bin
│       │   └── git-cz.js
│       ├── binaries
│       │   ├── cli-linux
│       │   ├── cli-macos
│       │   └── cli-win.exe
│       ├── dist
│       │   ├── cli.js
│       │   └── cz.js
│       ├── package.json
│       └── renovate.json
├── package.json
└── yarn.lock
```

## コーディングをするときの流れ

lab ディレクトリ内に実験（学習）したい内容のフォルダを作成する.

```
go mod init <フォルダ名>
```

## Git のフローについて.

### GitLab 上での操作

1. ISSUE の作成.
1. ISSUE からマージリクエストの作成.
1. マージリクエストの際にサブブランチを作成する.

### Local 上での操作

1. 最新状態のリモートリポジトリを pull する. ` git pull`
1. サブブランチに切り替える. ` git checkout <ブランチ名>`
1. コーディング
1. ステージング ` git add lab`
1. コミット ` git cz`
1. プッシュ ` git push origin <ブランチ名>`

### GitLab 上での操作

1. マージリクエストにてレビュー
1. ドラフトを外す
1. マージリクエストを承認

# 実験

## ポインタの実験

### Dir 構成について

```
.
├── functions
│   └── pointer.go
├── go.mod
└── main
    └── main.go
```

### 実験内容

### `pointer.go`

```golang
package functions

import "fmt"

//メモリアドレスをレスポンスする関数.
func ResponseRamAddress() *int {
	//Responseをint型で定義
	var Response int = 1

	//Responseを表示
	fmt.Printf("Response: %d\n",Response)

	//Responseのメモリアドレス
	ResponseAddress := &Response

	//Responseのメモリアドレスを表示
	fmt.Printf("ResponseAddress: %p\n", ResponseAddress)

	//intのポインタ型を返す
	return &Response
}
```

### `main.go`

```golang
package main

import (
	"fmt"
	"pointer_lab/functions"
)

func main() {
	//ResponseRamAddress関数を実行
	receiveResponse := functions.ResponseRamAddress()

	//receiveResponse: ポインタ型 メモリアドレスから値を参照する.
	fmt.Printf("*receiveResponse: %d\n",*receiveResponse)

	//変数のメモリアドレスを参照して値を書き換える.
	*receiveResponse = 2

	//書き換えた後の値を参照する.
	fmt.Printf("*receiveResponse: %d\n",*receiveResponse)
}
```

### 実行結果

```
╰─ go run main/main.go
Response: 1
ResponseAddress: 0x1400009c018
*receiveResponse: 1
*receiveResponse: 2
```

### 重要な点

- `&変数名`でメモリアドレスを返す.
- `*変数 = 値`でメモリアドレスを参照して変数の値を書き換えることができる.
  - このとき新しく変数を作成する必要はないのでメモリを少なくできる.(今回はわざわざ receiveResponse を作成しているのであまりメモリを減らせていないかも)
- `*変数`でメモリアドレスから値を参照することができる.

## 構造体の実験

### Dir 構成について

```
.
├── go.mod
├── main
│   └── main.go
└── structs
    └── struct.go

3 directories, 3 files
```

### 実験内容

### `struct.go`

```golang
package structs

// Person is a struct that represents a person
type Person struct {
	Name string
	Age  int
}

// PersonInterface is an interface for the Person struct
type PersonInterface interface {
	SetName(name string)
	SetAge(age int)
}

// NewPerson is a constructor for Person
func NewPerson(name string, age int) *Person {
	return &Person{name, age}
}

// GetName returns the name of the person
func (p *Person) SetName(name string) {
	p.Name = name
}

// GetAge returns the age of the person
func (p *Person) SetAge(age int) {
	p.Age = age
}
```

### `main.go`

```golang
package main

import (
	"fmt"
	"struct_lab/structs"
)

func main() {
	// Create a new person
	var human structs.Person

	// Set the name and age of the person
	human.Name = "ryosuke"
	human.Age = 22

	// Print the name and age of the person
	fmt.Printf("human.Name: %s human.Age: %d \n",human.Name, human.Age)

	// Create a new person
	p := structs.NewPerson("John", 25)

	// Print the name and age of the person
	fmt.Printf("p.Name: %s p.Age: %d \n",p.Name, p.Age)

	// Set the name of the person
	p.SetName("Doe")

	// Set the age of the person
	p.SetAge(30)

	// Print the name and age of the person
	fmt.Printf("p.Name: %s p.Age: %d \n",p.Name, p.Age)
}
```

### 実行結果

```
╰─ go run main/main.go
human.Name: ryosuke human.Age: 22
p.Name: John p.Age: 25
p.Name: Doe p.Age: 30
```

### 重要な点

- 構造体の宣言

```golang
type <構造体名> struct {
  <フィールド名01> <データ型>
  <フィールド名02> <データ型>
  .
  .
  .
}
```

構造体名の頭を大文字にすると別ファイルでも呼び出すことが可能である.

その為基本的には大文字で宣言する.

他ファイルで使って欲しくない時には小文字にすること推奨.

- 構造体の初期化方法

1. 【基本】変数定義後にフィールドを設定する方法
2. 【基本】{}で順番にフィールドの値を渡す方法
3. 【基本】フィールド名を`:`で指定する方法
4. 【応用】コンストラクタを使用して設定する

変数定義後にフィールドを設定する方法

```golang
var p Person
p.Name = hogehoge
p.Age = 1000
```

{}で順番にフィールドの値を渡す方法

```golang
p := Person{"John", 22}
```

フィールド名を`:`で指定する方法

```golang
p := Person{Age: 22, Name: "Bob"
```

コンストラクタを設定して設定する

```golang
func NewPerson(name string, age int) *Person {
  return &Person{name, age)
}

func main() {
  p := NewPerson("tanita", 20)
}
```

## モジュールの実験

### Dir 構成について

```
.
├── go.mod
├── main
│   ├── main.go
│   └── subMain.go
└── modules
    └── module.go

3 directories, 4 files
```

### 実験内容

### `module.go`

```golang
package modules

import "fmt"

func SpeakModulesFunction() {
	// Print a message
	fmt.Println("Hello, I am a SpeakModulesFunction from the modules package.")
}
```

### `subMain.go`

```golang
package main

import "fmt"

func SpeakSubMainFunction() {
  fmt.Println("Hello, I am a subMain function from the main package.")
}
```

### `main.go`

```golang
package main

import (
	"fmt"
	"module_lab/modules"
)

func main() {
	// Print a message
	fmt.Println("Hello, I am a main function from the main package.")

	// Call the SpeakSubMainFunction from the main package
	SpeakSubMainFunction()

	// Call the SpeakModulesFunction from the modules package
	modules.SpeakModulesFunction()
}
```

### 実行結果

```
╰─ go run main/main.go main/subMain.go
Hello, I am a main function from the main package.
Hello, I am a subMain function from the main package.
Hello, I am a SpeakModulesFunction from the modules package.
```

### 重要な点

- package 名は帰属するフォルダ名にする
- 同じ package 内であれば関数を呼び出す際に import する必要はない.
- 別 package から関数を呼び出す際には import で package 名を記述する必要がある.
- main パッケージにて関数を別ファイルに分けている場合は実行する際に注意が必要.

```
go run main/main.go main/subMain.go
```

というように run するファイルを記述する必要がある.

予測にはなってしまうが golang では main で import された package に関しては run されているが import されていない go ファイルは run されず main.go で呼び出した際に undifined になってしまうと考えられる.
